# CI-CD With Docker, Jenkins Pipelines
# Training Building a Modern CI/CD Pipeline with Jenkins

- https://app.pluralsight.com/course-player?clipId=57a92ff5-273a-4d4e-85c4-bae6dbdbf8c1

# Project Build a Docker Jenkins Pipeline to Implement CI/CD Workflow.

- https://lms.simplilearn.com/courses/3045/DevOps-Certification-training/assessment



# Sources

- https://www.cyberciti.biz/faq/how-to-install-docker-on-amazon-linux-2/
- https://www.liatrio.com/blog/building-with-docker-using-jenkins-pipelines
- https://www.pulumi.com/registry/packages/aws/how-to-guides/aws-ts-k8s-voting-app/
- https://github.com/hbollon/k8s-voting-app-aws

# How To Push a Docker Image To Docker Hub Using Jenkins
- https://blog.knoldus.com/how-to-push-a-docker-image-to-docker-hub-using-jenkins/ 

# Setup Jenkins in Docker container
- https://blog.knoldus.com/setup-jenkins-in-docker-container/   


# Integrating Vulnerability Scanners

# Trivy
- https://foreops.com/blog/trivy-intro/
- https://github.com/aquasecurity/trivy
- https://github.com/GandhiCloudLab/devsecops-with-trivy
- https://semaphoreci.com/blog/continuous-container-vulnerability-testing-with-trivy

# Anchore
# Integrate Anchore Scanning into Jenkins Pipeline
- https://anchore.com/blog/integrating-anchore-scanning-into-jenkins-pipeline-via-jenkinsfile/
- https://plugins.jenkins.io/anchore-container-scanner/

